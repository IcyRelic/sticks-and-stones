package me.icyrelic.com.Commands;



import java.util.HashMap;
import java.util.UUID;
import me.icyrelic.com.SticksAndStones;
import me.icyrelic.com.Arenas.ArenaData;
import me.icyrelic.com.Arenas.ArenaManager;
import me.icyrelic.com.Data.language;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class command_sas implements CommandExecutor {
	
	String noPerm = language.format(language.error_no_permission, "", "", "", 0, 0, 0);
	SticksAndStones plugin;
	public command_sas(SticksAndStones instance) {

		plugin = instance;

	}
	private HashMap<UUID, String> editing = new HashMap<UUID, String>();
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		

		if (cmd.getName().equalsIgnoreCase("sticksandstones")) {
			
			if(sender instanceof Player){
				
				Player p = (Player) sender;
				
				if(args.length >= 1){
					
					if(args[0].equalsIgnoreCase("help")){

						int page = 1;
						
						if(args.length == 2){
							if(isInteger(args[1])){
								page = Integer.parseInt(args[1]);
							}
						}
						
						if(page == 1){
							showHelp(1,p);
						}else if(page == 2){
							showHelp(2,p);
						}else{
							showHelp(1,p);
						}
						
						
						
						
					}else if(args[0].equalsIgnoreCase("join")){
						
						if(p.hasPermission("sas.join")){
							
							if(args.length <= 2){
								
								if(ArenaManager.getManager().getPlayersArena(p) == null){
									String arenaName = plugin.getConfig().getString("default_arena");
									
									if(args.length == 2){
										arenaName = args[1];
									}
									
									ArenaManager.getManager().addPlayers(plugin,p, arenaName);
								}else{
									p.sendMessage(language.format(language.error_already_in_arena, "", "", "", 0, 0, 0));
								}
							}else{
								p.sendMessage(language.format(language.usage_sas_join, "", "", "", 0, 0, 0));
							}
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}else if(args[0].equalsIgnoreCase("leave")){
						
						if(p.hasPermission("sas.leave")){
								
							if(ArenaManager.getManager().getPlayersArena(p) != null){
								
								String arenaName = ArenaManager.getManager().getPlayersArena(p).getName();
								ArenaManager.getManager().removePlayer(p, arenaName);
								
								if(ArenaManager.getManager().getArena(arenaName).getPlayers().isEmpty()){
									ArenaManager.getManager().endArena(arenaName);
								}
								
							}else{
								p.sendMessage(language.format(language.error_not_in_arena, "", "", "", 0, 0, 0));
							}
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}else if(args[0].equalsIgnoreCase("ready")){
						
						if(p.hasPermission("sas.ready")){
							
							if(ArenaManager.getManager().getPlayersArena(p) != null){
								
								String arenaName = ArenaManager.getManager().getPlayersArena(p).getName();
								ArenaManager.getManager().getArena(arenaName).setReady(p.getUniqueId(), true);
								
								if(ArenaManager.getManager().getArena(arenaName).allReady()){
									ArenaManager.getManager().startArenaCountdown(plugin,arenaName);
								}
								
							}else{
								p.sendMessage(language.format(language.error_not_in_arena, "", "", "", 0, 0, 0));
							}
							
						}else{
							p.sendMessage(noPerm);
						}
						
					}else if(args[0].equalsIgnoreCase("arena")){
						

						if(args.length >= 2){
							if(args[1].equalsIgnoreCase("create")){
								if(args.length == 3){
									if(p.hasPermission("sas.arena.create")){
										if(ArenaManager.getManager().getArena(args[2]) == null){
											
											if(editing.containsKey(p.getUniqueId())){
												editing.remove(p.getUniqueId());
											}
											editing.put(p.getUniqueId(), args[2]);
											@SuppressWarnings("unused")
											ArenaData ad = new ArenaData(args[2]);
											p.sendMessage(language.format(language.arena_created, args[2], "", "", 0, 0, 0));
											p.sendMessage(language.format(language.arena_checkdata_message, args[2], "", "", 0, 0, 0));
										}else{
											p.sendMessage(language.format(language.error_arena_already_exists, "", "", "", 0, 0, 0));
										}
									}else{
										p.sendMessage(noPerm);
									}
								}else{
									p.sendMessage(language.format(language.usage_sas_arena_create, "", "", "", 0, 0, 0));
								}
								
								
							}else if(args[1].equalsIgnoreCase("delete")){
								
								if(args.length == 3){
									if(p.hasPermission("sas.arena.delete")){
										if(ArenaManager.getManager().getArena(args[2]) != null){
											
											if(editing.containsKey(p.getUniqueId())){
												editing.remove(p.getUniqueId());
											}
											ArenaManager.getManager().deleteArena(args[2]);
											p.sendMessage(language.format(language.arena_deleted, "", "", "", 0, 0, 0));
										}else{
											p.sendMessage(language.format(language.error_arena_not_found, "", "", "", 0, 0, 0));
										}
									}else{
										p.sendMessage(noPerm);
									}
								}else{
									p.sendMessage(language.format(language.usage_sas_arena_delete, "", "", "", 0, 0, 0));
								}
								
								
							}else if(args[1].equalsIgnoreCase("edit")){
								
								if(args.length == 3){
									if(p.hasPermission("sas.arena.edit")){
									if(ArenaManager.getManager().getArena(args[2]) != null){
										
										if(editing.containsKey(p.getUniqueId())){
											editing.remove(p.getUniqueId());
										}
										
										editing.put(p.getUniqueId(), args[2]);
										p.sendMessage(language.format(language.arena_editing, "", "", "", 0, 0, 0));
										p.sendMessage(language.format(language.arena_checkdata_message, "", "", "", 0, 0, 0));
									}else{
										p.sendMessage(language.format(language.error_arena_not_found, "", "", "", 0, 0, 0));
									}
								}else{
									p.sendMessage(noPerm);
								}
								}else{
									p.sendMessage(language.format(language.usage_sas_arena_edit, "", "", "", 0, 0, 0));
								}
								
							}else if(args[1].equalsIgnoreCase("checkdata")){
								if(p.hasPermission("sas.arena.edit")){
									if(editing.containsKey(p.getUniqueId())){
										String arenaName = editing.get(p.getUniqueId());
										p.sendMessage(language.format(language.arena_checkdata_header, arenaName, "", "", 0, 0, 0));
										p.sendMessage(language.format(language.arena_checkdata_settings, "", "", ArenaManager.getManager().getArenaData(arenaName).getSettings(), 0, 0, 0));
										p.sendMessage(language.format(language.arena_checkdata_warps, "", "", ArenaManager.getManager().getArenaData(arenaName).getWarps(), 0, 0, 0));
										p.sendMessage(language.format(language.arena_checkdata_spawnpoints, "", "", ArenaManager.getManager().getArenaData(arenaName).getSpawnpoints().size()+"/"+ArenaManager.getManager().getArenaData(arenaName).getMaxPlayers(), 0, 0, 0));
										
										
										
									}else{
										p.sendMessage(language.format(language.error_not_editing, "", "", "", 0, 0, 0));
									}
								}
							}else if(args[1].equalsIgnoreCase("set")){
								
								if(p.hasPermission("sas.arena.edit")){
									
									if(editing.containsKey(p.getUniqueId())){
										String arenaName = editing.get(p.getUniqueId());
										if(args.length >= 3){
											if(args[2].equalsIgnoreCase("lobby")){
												
												ArenaManager.getManager().getArenaData(arenaName).setWarpLobby(p.getLocation());
												p.sendMessage(language.format(language.arena_set_lobby, arenaName, "", "", 0, 0, 0));
												p.sendMessage(language.format(language.arena_checkdata_message, "", "", "", 0, 0, 0));
												tryCreate(arenaName, p);
												
											}else if(args[2].equalsIgnoreCase("spawnpoint")){
												
												ArenaManager.getManager().getArenaData(arenaName).addSpawnpoint(p.getLocation());
												p.sendMessage(language.format(language.arena_set_spawnpoint, arenaName, "", "", 0, 0, 0));
												p.sendMessage(language.format(language.arena_checkdata_message, "", "", "", 0, 0, 0));
												tryCreate(arenaName, p);
												
											}else if(args[2].equalsIgnoreCase("end")){
												
												ArenaManager.getManager().getArenaData(arenaName).setWarpEnd(p.getLocation());
												p.sendMessage(language.format(language.arena_set_end, arenaName, "", "", 0, 0, 0));
												p.sendMessage(language.format(language.arena_checkdata_message, "", "", "", 0, 0, 0));
												tryCreate(arenaName, p);
												
											}else if(args[2].equalsIgnoreCase("minplayers")){
												
												if(args.length == 4){
													if(isInteger(args[3])){
														ArenaManager.getManager().getArenaData(arenaName).setMinPlayers(Integer.parseInt(args[3]));
														p.sendMessage(language.format(language.arena_set_minplayers, arenaName, "", "", 0, 0, 0));
														p.sendMessage(language.format(language.arena_checkdata_message, "", "", "", 0, 0, 0));
														tryCreate(arenaName, p);
													}else{
														p.sendMessage(language.format(language.error_must_be_int, "", "", "", 0, 0, 0));
													}
												}else{
													p.sendMessage(language.format(language.usage_sas_arena_set_minplayers, "", "", "", 0, 0, 0));
												}
												
											}else if(args[2].equalsIgnoreCase("maxplayers")){
												if(args.length == 4){
													if(isInteger(args[3])){
														ArenaManager.getManager().getArenaData(arenaName).setMaxPlayers(Integer.parseInt(args[3]));
														p.sendMessage(language.format(language.arena_set_maxplayers, arenaName, "", "", 0, 0, 0));
														p.sendMessage(language.format(language.arena_checkdata_message, "", "", "", 0, 0, 0));
														tryCreate(arenaName, p);
													}else{
														p.sendMessage(language.format(language.error_must_be_int, "", "", "", 0, 0, 0));
													}
												}else{
													p.sendMessage(language.format(language.usage_sas_arena_set_maxplayers, "", "", "", 0, 0, 0));
												}
											}else if(args[2].equalsIgnoreCase("reward")){
												if(args.length == 5){
													if(args[3].equalsIgnoreCase("money")){
														if(isInteger(args[4])){
															ArenaManager.getManager().getArenaData(arenaName).setMoneyReward(Integer.parseInt(args[4]));
															p.sendMessage(language.format(language.arena_set_reward_money, arenaName, "", "", 0, 0, 0));
															p.sendMessage(language.format(language.arena_checkdata_message, "", "", "", 0, 0, 0));
															tryCreate(arenaName, p);
														}else{
															p.sendMessage(language.format(language.error_must_be_int, "", "", "", 0, 0, 0));
														}
													}else{
														p.sendMessage(language.format(language.usage_sas_arena_set_reward_money, "", "", "", 0, 0, 0));
													}
												}else{
													p.sendMessage(language.format(language.usage_sas_arena_set_reward_money, "", "", "", 0, 0, 0));
												}
											}else{
												p.sendMessage(language.format(language.usage_sas_arena_set, "", "", "", 0, 0, 0));
											}
										}else{
											p.sendMessage(language.format(language.usage_sas_arena_set, "", "", "", 0, 0, 0));
										}
											
									}else{
										p.sendMessage(language.format(language.error_not_editing, "", "", "", 0, 0, 0));
									}
									
								}else{
									p.sendMessage(noPerm);
								}
								
							}else{
								p.sendMessage(language.format(language.usage_sas_arena, "", "", "", 0, 0, 0));
							}
						}else{
							p.sendMessage(language.format(language.usage_sas_arena, "", "", "", 0, 0, 0));
						}
						
					}else if(args[0].equalsIgnoreCase("list")){
						
						if(p.hasPermission("sas.list")){
							String list = ArenaManager.getManager().listArenas().toString().replace("]", "").replace("[", "");
							if(list.equals("")){
								list = language.arena_list_no_arenas;
							}
							p.sendMessage(language.format(language.arena_list_header, "", "", "", 0, 0, 0));
							p.sendMessage(language.format(language.arena_list_list, "", "", list, 0, 0, 0));
						}else{
							p.sendMessage(noPerm);
						}
						
					}else if(args[0].equalsIgnoreCase("players")){
						if(p.hasPermission("sas.list")){
							
							if(ArenaManager.getManager().getPlayersArena(p) != null){
								String arenaName = ArenaManager.getManager().getPlayersArena(p).getName();
								String list = ArenaManager.getManager().getArena(arenaName).getPlayers().toString().replace("]", "").replace("[", "");
								if(list.equals("")){
									list = language.arena_players_no_players;
								}
								p.sendMessage(language.format(language.arena_players_header, "", "", "", 0, 0, 0));
								p.sendMessage(language.format(language.arena_players_list, "", "", list, 0, 0, 0));
							}else{
								p.sendMessage(language.format(language.error_not_in_arena, "", "", "", 0, 0, 0));
							}
						}else{
							p.sendMessage(noPerm);
						}
						
						
					}else if(args[0].equalsIgnoreCase("force")){
						if(p.hasPermission("sas.arena.force")){
							if(args.length == 3){
								String arenaName = args[2];
								if(args[1].equalsIgnoreCase("start")){
									ArenaManager.getManager().startArenaCountdown(plugin,arenaName);
									p.sendMessage(language.format(language.arena_force_start, "", "", "", 0, 0, 0));
									
								}else if(args[1].equalsIgnoreCase("end")){
									ArenaManager.getManager().endArena(arenaName);
									p.sendMessage(language.format(language.arena_force_end, "", "", "", 0, 0, 0));
								}else{
									p.sendMessage(language.format(language.usage_sas_force, "", "", "", 0, 0, 0));
								}
								
								
							}else{
								p.sendMessage(language.format(language.usage_sas_force, "", "", "", 0, 0, 0));
							}
						}else{
							p.sendMessage(noPerm);
						}
						
					}else{
						p.sendMessage(language.format(language.error_unknown_command, "", "", "", 0, 0, 0));
					}
					
				}else{
					p.sendMessage(language.Colorize(language.plugin_information.replace("%pluginName%", plugin.getDescription().getName()).replace("%version%", plugin.getDescription().getVersion())));
					
				}
				
			}else{
				sender.sendMessage(language.format(language.error_not_player, "", "", "", 0, 0, 0));
			}
			
			
		}
		return true;
	}
	
	public void showHelp(int page, Player p){
		if(page == 1){
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GRAY +"===========     "+ChatColor.GOLD+"Sticks  and  Stones   "+ChatColor.GRAY +" ==========");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas help [page]");
			p.sendMessage(ChatColor.BLUE+"- Shows this help screen");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas arena");
			p.sendMessage(ChatColor.BLUE+"- Manage Arenas");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas arena set");
			p.sendMessage(ChatColor.BLUE+"- Set/Edit Settings for Arenas");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas arena edit");
			p.sendMessage(ChatColor.BLUE+"- Select an arena to edit");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas join [name]");
			p.sendMessage(ChatColor.BLUE+"- Join an arena");
			p.sendMessage(ChatColor.BLUE+"- if no name is supplied it uses the name 'default'");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.DARK_GREEN+"/sas help 2 to show the next page");
		}
		if(page == 2){
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage("");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GRAY +"===========     "+ChatColor.GOLD+"Sticks  and  Stones   "+ChatColor.GRAY +" ==========");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas leave]");
			p.sendMessage(ChatColor.BLUE+"- Leave an arena");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas list");
			p.sendMessage(ChatColor.BLUE+"- List all available arenas");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas players");
			p.sendMessage(ChatColor.BLUE+"- List players in arena");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.GREEN+"/sas force");
			p.sendMessage(ChatColor.BLUE+"- Force an arena to start/end");
			p.sendMessage(ChatColor.GRAY +"===========================================");
			p.sendMessage(ChatColor.DARK_GREEN+"/sas help 3 to show the next page");
		}
	}
	
	public void tryCreate(String arenaName, Player p){
		if(ArenaManager.getManager().getArenaData(arenaName).allEntered() && ArenaManager.getManager().getArena(arenaName) == null){
			Location warp_lobby = ArenaManager.getManager().getArenaData(arenaName).getWarpLobby();
			Location warp_end = ArenaManager.getManager().getArenaData(arenaName).getWarpEnd();
			int maxPlayers  = ArenaManager.getManager().getArenaData(arenaName).getMaxPlayers();
			int minPlayers  = ArenaManager.getManager().getArenaData(arenaName).getMinPlayers();
			int money  = ArenaManager.getManager().getArenaData(arenaName).getMoneyReward();
			HashMap<String, Location> spawnpoints = ArenaManager.getManager().getArenaData(arenaName).getSpawnpoints();
			
			if(spawnpoints.size() >= maxPlayers && maxPlayers != 0){
				ArenaManager.getManager().createArena(arenaName, warp_lobby, spawnpoints, warp_end, maxPlayers, minPlayers, money);
				p.sendMessage(language.format(language.arena_fully_setup, arenaName, "", "", 0, 0, 0));
			}
		}
	}
	
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    return true;
	}
	
}
