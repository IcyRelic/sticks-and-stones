package me.icyrelic.com.Arenas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
 
public class Arena {
 
	public static ArrayList<Arena> arenaObjects = new ArrayList<Arena>();
 
	private Location warp_lobby, warp_end;
 
	private String name; 
	private int money;
	private ArrayList<String> players = new ArrayList<String>();
	private ArrayList<String> dead = new ArrayList<String>();
	private ArrayList<String> usedSpawns = new ArrayList<String>();
	private HashMap<UUID, Boolean> ready = new HashMap<UUID, Boolean>();
	private HashMap<UUID, Integer> score = new HashMap<UUID, Integer>();
	private HashMap<String, Location> spawnpoints = new HashMap<String, Location>();
	private int maxPlayers;
	private int minPlayers;
	
	private boolean countdown = false;
	private boolean inGame = false;
	
	
	public Arena (String arenaName, Location warp_lobby, HashMap<String, Location> spawnpoints, Location warp_end, int maxPlayers, int minPlayers, int money) {
		this.name = arenaName;
		this.warp_lobby = warp_lobby;
		this.warp_end = warp_end;
		this.maxPlayers = maxPlayers;
		this.minPlayers = minPlayers;
		this.spawnpoints = spawnpoints;
		this.money = money;
		arenaObjects.add(this);
	}
	
	public Integer getMoneyReward(){
		return money;
	}
	
	public Integer getScore(UUID uid){
		return this.score.get(uid);
	}
	public void setScore(UUID uid, Integer score){
		if(this.score.containsKey(uid)){
			this.score.remove(uid);
		}
		this.score.put(uid, score);
	}
	public void removeScore(UUID uid){
		if(this.score.containsKey(uid)){
			this.score.remove(uid);
		}
	}
	public void addToScore(UUID uid, Integer number){
		int score = this.score.get(uid);
		if(this.score.containsKey(uid)){
			this.score.remove(uid);
		}
		this.score.put(uid, score+number);
	}
	public HashMap<UUID, Integer> getAllScores() {
		return this.score;
	}
	public HashMap<String, Location> getSpawnpoints() {
		return this.spawnpoints;
	}
	
	public Location getSpawnpoint(String id){
		return this.spawnpoints.get(id);
	}
	public void setSpawnpoints(HashMap<String, Location> spawnpoints){
		this.spawnpoints = spawnpoints;
	}
	public String getRandomSpawnpoint(){
		Object[] spawns = spawnpoints.keySet().toArray();
        Object spawnpoint = spawns[new Random().nextInt(spawns.length)];
		return (String) spawnpoint;
	}
	
	public Location getLobbyLocation() {
		return this.warp_lobby;
	}
 
	public void setLobbyLocation(Location warp_lobby) {
		this.warp_lobby = warp_lobby;
	}
 
	public Location getEndLocation() {
		return this.warp_end;
	}
 
	public void setEndLocation(Location warp_end) {
		this.warp_end = warp_end;
	}
 
	public String getName() {
		return this.name;
	}
 
	public void setName(String name) {
		this.name = name;
	}
 
	public int getMaxPlayers() {
		return this.maxPlayers;
	}
	
	public int getMinPlayers() {
		return this.minPlayers;
	}
 
	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	
	public void setMinPlayers(int minPlayers) {
		this.minPlayers = minPlayers;
	}
 
	public ArrayList<String> getPlayers() {
		return this.players;
	}
	public ArrayList<String> getDead() {
		return this.dead;
	}
	public ArrayList<String> getUsedSpawns() {
		return this.usedSpawns;
	}
 
 
	public boolean isFull() { 
		if (players.size() >= maxPlayers) {
			return true;
		} else {
			return false;
		}
	}
 
 
	public boolean isInGame() {
		return inGame;
	}
	
	public boolean isCountdown() {
		return countdown;
	}
	
	public boolean allReady() {
		boolean all_ready = true;
		
		for(final Entry<UUID,Boolean> entry : (Set<Entry<UUID,Boolean>>)ready.entrySet()) {
			if(entry.getValue() == false){
				all_ready = false;
			}
		}
		
		return all_ready;
	}
	
	public boolean setReady(UUID uid, boolean status) {
		if(ready.containsKey(uid)){
			ready.remove(uid);
		}
		ready.put(uid, status);
		return status;
	}
 
	public void setCountdown(boolean countdown) {
		this.countdown = countdown;
	}
	
	public void setInGame(boolean inGame) {
		this.inGame = inGame;
	}
 
	@SuppressWarnings("deprecation")
	public void sendMessage(String message) {
		for (String s: players) {
			Bukkit.getPlayer(s).sendMessage(message);
		}
	}
 
 
}