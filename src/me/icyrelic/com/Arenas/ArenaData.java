package me.icyrelic.com.Arenas;

import java.util.ArrayList;
import java.util.HashMap;


import org.bukkit.Location;

public class ArenaData {
	
	public static ArrayList<ArenaData> dataObjects = new ArrayList<ArenaData>();
	private HashMap<String, Location> spawnpoints = new HashMap<String, Location>();
	private final String arenaName;
	private int money = 0;
	private Location warp_lobby = null;
    private Location warp_end = null;
    private int maxPlayers = 0;
    private int minPlayers = 0;
    private int numSpawns = 0;
    public ArenaData(String arenaName){
        this.arenaName = arenaName;
		dataObjects.add(this);
	}

	public Boolean allEntered(){
		
		
		if(getWarps().equals("") && getSettings().equals("") && spawnpoints.size() >= maxPlayers){
			return true;
		}else{
			return false;
		}
		
	}
	public void setMoneyReward(int money){
    	this.money = money;
    }
	public int getMoneyReward(){
    	return this.money;
    }
    public void setWarpLobby(Location loc){
    	this.warp_lobby = loc;
    }
    public void setWarpEnd(Location loc){
    	this.warp_end = loc;
    }
    public void setMaxPlayers(Integer max){
    	this.maxPlayers = max;
    }
    public void setMinPlayers(Integer min){
    	this.minPlayers = min;
    }
    public void addSpawnpoint(Location loc){
    	
    	
    	numSpawns++;
    	String spawnid = "s"+numSpawns;
    	spawnpoints.put(spawnid, loc);
    	
    	
    }
    
    public HashMap<String, Location> getSpawnpoints(){
    	return this.spawnpoints;
    }
    
    public String getName(){
    	return this.arenaName;
    }
    
    public Location getWarpLobby(){
    	return this.warp_lobby;
    }
    
    public Location getWarpEnd(){
    	return this.warp_end;
    }
    
    public int getMaxPlayers(){
    	return this.maxPlayers;
    }
    
    public int getMinPlayers(){
    	return this.minPlayers;
    }
    public String getWarps(){
    	String warps = "";
    	if(warp_lobby == null){
			if(!warps.equals("")){
				warps = warps+", ";
			}
			warps = warps+"lobby";
		}
		if(warp_end == null){
			if(!warps.equals("")){
				warps = warps+", ";
			}
			warps = warps+"end";
		}
    	return warps;
    }
    
    public String getSettings(){
		String settings = "";
		if(maxPlayers == 0){
			if(!settings.equals("")){
				settings = settings+", ";
			}
			settings = settings+"maxPlayers";
		}
		if(minPlayers == 0){
			if(!settings.equals("")){
				settings = settings+", ";
			}
			settings = settings+"minPlayers";
		}
		
		if(money == 0){
			if(!settings.equals("")){
				settings = settings+", ";
			}
			settings = settings+"money";
		}
		return settings;
    }
    
	public String getNeededSpawnpoints(){
		String spawns = "";
		/** Spawns **/
		
		int x = this.maxPlayers - spawnpoints.size();
		
		spawns = x+"";
		
        return spawns;
	}
}
