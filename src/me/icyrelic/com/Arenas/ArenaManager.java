package me.icyrelic.com.Arenas;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;

import me.icyrelic.com.CleanItem;
import me.icyrelic.com.Gameplay;
import me.icyrelic.com.RunableCountdown;
import me.icyrelic.com.SticksAndStones;
import me.icyrelic.com.Data.language;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;


public class ArenaManager {
   
	private HashMap<UUID, ItemStack[]> pInv = new HashMap<UUID, ItemStack[]>();
	private HashMap<UUID, ItemStack[]> pArmor = new HashMap<UUID, ItemStack[]>();
	private HashMap<UUID, Integer> pExp = new HashMap<UUID, Integer>();
	private static ArrayList<String> arenaList = new ArrayList<String>();
	static SticksAndStones plugin;
	public ArenaManager(SticksAndStones instance){
		plugin = instance;
	}
	private static ArenaManager am = new ArenaManager(plugin);
	
    public static ArenaManager getManager() {
        return am;
    }
   
    public Arena getArena(String name) {
        for (Arena a: Arena.arenaObjects) { 
            if (a.getName().equals(name)) { 
                return a; 
            }
        }
        return null; 
    }
    
    public ArrayList<String> listArenas() {
    	return arenaList;
    }
    
    public ArenaData getArenaData(String name) {
    	for (ArenaData a: ArenaData.dataObjects) { 
                if(a.getName().equals(name)){
                	return a;
                }
        }
        return null; 
    }
   
    public void startArenaCountdown(SticksAndStones instance, String arenaName) {
    	getArena(arenaName).setCountdown(true);
		@SuppressWarnings("unused")
		BukkitTask task = new RunableCountdown(ArenaManager.getManager().getArena(arenaName).getPlayers(), 10, arenaName, instance).runTaskTimer(instance, 0, 20);
    }
   
    public void addPlayers(SticksAndStones instance, Player player, String arenaName) {
       
        if (getArena(arenaName) != null) { 
            Arena arena = getArena(arenaName); 
           
            if (!arena.isFull()) { 
               
                if (!arena.isInGame()) {
                	// Save inventory //
                	
                	pInv.put(player.getUniqueId(), player.getInventory().getContents());
                	pArmor.put(player.getUniqueId(), player.getInventory().getArmorContents());
                	pExp.put(player.getUniqueId(), player.getLevel());
                	player.setLevel(0);
                    player.getInventory().clear(); 
                    clearArmor(player);
                    player.setHealth(player.getMaxHealth());
                    player.setFoodLevel(20);
                    player.setFireTicks(0);
                   
                    player.teleport(arena.getLobbyLocation());
                   
                    arena.getPlayers().add(player.getName());
                    arena.setReady(player.getUniqueId(), true);
                    arena.sendMessage(language.format(language.arena_player_joined, arena.getName(), player.getName(), "", 0, 0, 0));
                    
                   
                    if (arena.getPlayers().size() == arena.getMinPlayers() && arena.allReady()) { 
                    	startArenaCountdown(instance, arenaName); 
                    }
                   
               
                } else { 
                	player.sendMessage(language.format(language.error_arena_ingame, "", "", "", 0, 0, 0));
                   
                }
            } else {
                player.sendMessage(language.format(language.error_arena_full, "", "", "", 0, 0, 0));
            }
           
        } else {
            player.sendMessage(language.format(language.error_arena_not_found, "", "", "", 0, 0, 0));
        }
       
    }
    

    
    
    public Arena getPlayersArena(Player player) {
    	
        for (Arena a: Arena.arenaObjects) { 
            if (a.getPlayers().contains(player.getName())) { 
                return a; 
            }
        }
        
        return null;
       
    }
   
   
    public void removePlayer(Player player, String arenaName) {
       
        if (getArena(arenaName) != null) {
           
             Arena arena = getArena(arenaName); 
           
            if (arena.getPlayers().contains(player.getName())) {
            	// Restore inventory //
            	
                player.getInventory().clear(); 
                clearArmor(player);
                
                if(pInv.containsKey(player.getUniqueId())){
                	player.getInventory().setContents(pInv.get(player.getUniqueId()));
            		pInv.remove(player.getUniqueId());
            	}
                if(pArmor.containsKey(player.getUniqueId())){
                	player.getInventory().setArmorContents(pArmor.get(player.getUniqueId()));
                	pArmor.remove(player.getUniqueId());
            	}
                if(pExp.containsKey(player.getUniqueId())){
                	player.setLevel(pExp.get(player.getUniqueId()));
            		pInv.remove(player.getUniqueId());
            	}
                player.removePotionEffect(PotionEffectType.INVISIBILITY);
                player.setHealth(player.getMaxHealth());
                player.setFlying(false);
                player.setFoodLevel(20);
                player.setFireTicks(0); 
                player.teleport(arena.getEndLocation());
                player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
                arena.removeScore(player.getUniqueId());
                arena.getPlayers().remove(player.getName()); 
                arena.sendMessage(language.format(language.arena_player_left, "", "", "", 0, 0, 0));
               
            } else {
                player.sendMessage(language.format(language.error_arena_not_found, "", "", "", 0, 0, 0));
            }
           
           
        } else { 
        	 player.sendMessage(language.format(language.error_arena_not_found, "", "", "", 0, 0, 0));
        }
    }
       
    public void respawnPlayer(String arenaName, Player p) {
    	Arena arena = getArena(arenaName);

    	if(arena.getDead().contains(p.getName())){
    		arena.getDead().remove(p.getName());
    	}
    	if(arena.isInGame()){
    		p.setFlying(false);
    		p.setAllowFlight(false);
        	p.removePotionEffect(PotionEffectType.INVISIBILITY);
        	String spawnid = arena.getRandomSpawnpoint();
           	p.teleport(arena.getSpawnpoint(spawnid));
           	p.getInventory().addItem(new CleanItem(Material.BOW)
            .withName("Crossbow")
            .amount(1)
            .withEnchantment(Enchantment.ARROW_INFINITE, 1, false)
            .withLore(ChatColor.GREEN + "1 Shot kill")
            .toItemStack());
            
           	

            p.getInventory().addItem(new CleanItem(Material.STONE_AXE)
            .withName("Tomahawk")
            .amount(1)
            .withLore(ChatColor.GREEN + "Humiliate a player")
            .withLore(ChatColor.ITALIC + "Right click to throw")
            .toItemStack());
            
            p.getInventory().addItem(new CleanItem(Material.ARROW)
            .amount(1)
            .toItemStack());
    	}
    }
    
    public void processKill(SticksAndStones instance, final String arenaName, Player killer, final Player killed){
    	Location loc = killed.getLocation();
		loc.setY(killed.getLocation().getBlockY()+1);
		killed.getLocation().getWorld().playEffect(loc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
    	killed.sendMessage(language.format(language.arena_player_died, "", "", "", 0, 0, 0));
    	killed.sendMessage(language.format(language.arena_player_died_respawn_in, "", "", "", 6, 0, 0));
    	killed.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 500, 50));
    	killed.getInventory().clear();
    	killed.setAllowFlight(true);
    	killed.setFlying(true);
    	Arena arena = getArena(arenaName);
    	arena.getDead().add(killed.getName());
    	arena.addToScore(killer.getUniqueId(), 10);
    	Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() { 
    		public void run() {
    			respawnPlayer(arenaName, killed);
    		}
	  	}, 120L);
    	
    	
    	
    }
	public void processHumiliation(SticksAndStones instance, final String arenaName, Player killer, final Player killed){
		
		Location loc = killed.getLocation();
		loc.setY(killed.getLocation().getBlockY()+1);
		killed.getLocation().getWorld().playEffect(loc, Effect.STEP_SOUND, Material.REDSTONE_WIRE);
		
		killed.sendMessage(language.format(language.arena_player_died, "", "", "", 0, 0, 0));
    	killed.sendMessage(language.format(language.arena_player_died_respawn_in, "", "", "", 6, 0, 0));
    	killed.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 500, 50));
    	killed.getInventory().clear();
    	killed.setAllowFlight(true);
    	killed.setFlying(true);
    	Arena arena = getArena(arenaName);
    	arena.getDead().add(killed.getName());
    	arena.setScore(killed.getUniqueId(), 0);
    	arena.addToScore(killer.getUniqueId(), 5);
    	Bukkit.getScheduler().scheduleSyncDelayedTask(instance, new Runnable() { 
    		public void run() {
    			respawnPlayer(arenaName, killed);
    		}
	  	}, 120L);
    	
	}
       
        @SuppressWarnings("deprecation")
		public void startArena(String arenaName, SticksAndStones instance) {
           
            if (getArena(arenaName) != null) {
                 
                 Arena arena = getArena(arenaName); 
                 
                 if(arena.getPlayers().size() >= arena.getMinPlayers()){
                     arena.sendMessage(language.format(language.arena_started, "", "", "", 0, 0, 0));
                     
                     arena.setInGame(true);
                     arena.setCountdown(false);
                     for (String name: arena.getPlayers()) {
                         Player p = Bukkit.getPlayer(name);
                         arena.setScore(p.getUniqueId(), 0);
                         String spawnid = arena.getRandomSpawnpoint();
                         if(arena.getUsedSpawns().contains(spawnid)){
                        	 for (Entry<String, Location> entry : arena.getSpawnpoints().entrySet()) {
                        		 if(!arena.getUsedSpawns().contains(entry.getKey())){
                        			 p.teleport(entry.getValue());
                        			 arena.getUsedSpawns().add(entry.getKey());
                        		 }
                        	 }
                         }else{
                        	 p.teleport(arena.getSpawnpoint(spawnid));
                        	 arena.getUsedSpawns().add(spawnid);
                         }
                         
                         
                         
                         p.getInventory().addItem(new CleanItem(Material.BOW)
                         .withName("Crossbow")
                         .withEnchantment(Enchantment.ARROW_INFINITE, 1, false)
                         .amount(1)
                         .withLore(ChatColor.GREEN + "1 Shot kill")
                         .toItemStack());
                         
                         

                         p.getInventory().addItem(new CleanItem(Material.STONE_AXE)
                         .withName("Tomahawk")
                         .amount(1)
                         .withLore(ChatColor.GREEN + "Humiliate a player")
                         .withLore(ChatColor.ITALIC + "Right click to throw")
                         .toItemStack());
                         
                         p.getInventory().addItem(new CleanItem(Material.ARROW)
                         .amount(1)
                         .toItemStack());

                     }
                     arena.getUsedSpawns().clear();
                     
                     @SuppressWarnings("unused")
    				BukkitTask task = new Gameplay(ArenaManager.getManager().getArena(arenaName).getPlayers(), 480, arenaName).runTaskTimer(instance, 0, 20);
                 }else{
                	 arena.sendMessage(language.format(language.error_arena_not_enough_players, "", "", "", 0, 0, 0));
                	 endArena(arena.getName());
                 }
                 
                 
             }
           
        }
       
       
        public void endArena(String arenaName) {
           
            if (getArena(arenaName) != null) {
            	
                 Arena arena = getArena(arenaName);
                 arena.sendMessage(language.format(language.arena_ended, "", "", "", 0, 0, 0));
                 arena.setInGame(false);
                 arena.setCountdown(false);
                 
                 Entry<UUID,Integer> maxEntry = null;

                 for(Entry<UUID, Integer> entry : arena.getAllScores().entrySet()) {
                     if (maxEntry == null || entry.getValue() > maxEntry.getValue()) {
                         maxEntry = entry;
                     }
                 }
                 Player winner = Bukkit.getPlayer(maxEntry.getKey());
                 arena.sendMessage(language.format(language.arena_winner, arenaName, winner.getName(), "", 0, 0, arena.getScore(maxEntry.getKey())));
                 Bukkit.getPlayer(maxEntry.getKey()).sendMessage(language.format(language.arena_give_reward_money, arenaName, winner.getName(), "", 0, arena.getMoneyReward(), arena.getScore(maxEntry.getKey())));
                 SticksAndStones.economy.depositPlayer(Bukkit.getPlayer(maxEntry.getKey()), maxEntry.getValue());
                 
                 Iterator<String> itr = arena.getPlayers().iterator();
                 
                 while (itr.hasNext()) {
                	 
                	 String name =  itr.next();
                	 
                	@SuppressWarnings("deprecation")
					Player player = Bukkit.getPlayer(name);
                	player.teleport(arena.getEndLocation());
            	    arena.removeScore(player.getUniqueId());  	 
                	player.getInventory().clear(); 
                	player.setFlying(false);
                	player.setAllowFlight(false);
                	player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
                	clearArmor(player);
                	player.removePotionEffect(PotionEffectType.INVISIBILITY);
                	if(pInv.containsKey(player.getUniqueId())){
                		player.getInventory().setContents(pInv.get(player.getUniqueId()));
                		
                	}
                	
                	if(pArmor.containsKey(player.getUniqueId())){
                    	player.getInventory().setArmorContents(pArmor.get(player.getUniqueId()));
                    	pArmor.remove(player.getUniqueId());
                	}
                	if(pExp.containsKey(player.getUniqueId())){
                    	player.setLevel(pExp.get(player.getUniqueId()));
                		pInv.remove(player.getUniqueId());
                	}
                    
                	
                	player.setHealth(player.getMaxHealth()); 
                	player.setFireTicks(0); 
               
                	itr.remove();
                 }
               
               
            }
        }
           
           
        public void loadArenas() {
            String path = "plugins/SticksAndStones/Arenas";
            File folder = new File(path);

           		 File[] arenas = folder.listFiles();

           		 int amount = arenas.length;

           		 System.out.println("[SticksAndStones] Loading "+amount+" arenas.");
           		 for(File Arena:arenas){
           			 
           			FileConfiguration ArenaConf = YamlConfiguration.loadConfiguration(Arena);
           			
           			String arenaName = ArenaConf.getString("Name");
           			String warp_lobbystr = ArenaConf.getString("location.warps.lobby");
           			String warp_endstr = ArenaConf.getString("location.warps.end");
           			int maxPlayers = ArenaConf.getInt("MaxPlayers");
           			int minPlayers = ArenaConf.getInt("MinPlayers");
           			int money = ArenaConf.getInt("rewards.money");
           			String[] lobby = warp_lobbystr.split(",");
           			World lobbyw = Bukkit.getWorld(lobby[0]);
           			
           			Location warp_lobby = new Location(lobbyw, Double.parseDouble(lobby[1]), Double.parseDouble(lobby[2]), Double.parseDouble(lobby[3]));
           			warp_lobby.setPitch(Float.parseFloat(lobby[4]));
           			warp_lobby.setYaw(Float.parseFloat(lobby[4]));
           			
           			String[] end = warp_endstr.split(",");
           			World endw =  Bukkit.getWorld(end[0]);
           			
           			Location warp_end = new Location(endw, Double.parseDouble(end[1]), Double.parseDouble(end[2]), Double.parseDouble(end[3]));
           			warp_end.setPitch(Float.parseFloat(end[4]));
           			warp_end.setYaw(Float.parseFloat(end[4]));
           			
           			HashMap<String, Location> spawnpoints = new HashMap<String, Location>();
           			
           			for(String key : ArenaConf.getConfigurationSection("location.spawnpoints").getKeys(false)) {
           				
           				String str = ArenaConf.getString("location.spawnpoints."+key);
           				
           				String[] spawn = str.split(",");
           				
           				World w = Bukkit.getWorld(spawn[0]);
           				int x = Integer.parseInt(spawn[1]);
           				int y = Integer.parseInt(spawn[2]);
           				int z = Integer.parseInt(spawn[3]);
           				float pitch = Float.parseFloat(spawn[4]);
           				float yaw = Float.parseFloat(spawn[5]);
           				
           				Location loc = new Location(w, x, y, z);
           				loc.setPitch(pitch);
           				loc.setYaw(yaw);
           				
           				
           				spawnpoints.put(key, loc);
           			}
           			
           			@SuppressWarnings("unused")
           			Arena arena = new Arena(arenaName, warp_lobby, spawnpoints, warp_end, maxPlayers, minPlayers, money);
           			ArenaData ad = new ArenaData(arenaName);
           			ad.setMaxPlayers(maxPlayers);
           			ad.setMinPlayers(minPlayers);
           			ad.setWarpEnd(warp_end);
           			ad.setWarpLobby(warp_lobby);
           			
           		 for (Entry<String, Location> entry : spawnpoints.entrySet()) {
            		 ad.addSpawnpoint(entry.getValue());
            	 }
           			
           			
           			arenaList.add(arenaName);
           		 }
        	
        	
        	
        	
        }
        
        
       
        public void createArena(String arenaName, Location warp_lobby, HashMap<String, Location> spawnpoints, Location warp_end, int maxPlayers, int minPlayers, int money) {
            
         File Arena;
       	 File ArenaFolder;
       	 ArenaFolder = new File("plugins/SticksAndStones", "Arenas");
       	 ArenaFolder.mkdir();
       	 Arena = new File(ArenaFolder, arenaName + ".yml");
       	 
       	 try {
   			Arena.createNewFile();
   		 } catch (IOException e) {
   			e.printStackTrace();
   		 }
       	 
       	 String lobby = (warp_lobby.getWorld().getName()+","+warp_lobby.getBlockX()+","+warp_lobby.getBlockY()+","+warp_lobby.getBlockZ()+","+warp_lobby.getPitch()+","+warp_lobby.getYaw());
       	 String end = (warp_end.getWorld().getName()+","+warp_end.getBlockX()+","+warp_end.getBlockY()+","+warp_end.getBlockZ()+","+warp_end.getPitch()+","+warp_end.getYaw());
       	 
       	 FileConfiguration ArenaConf = YamlConfiguration.loadConfiguration(Arena);
       	 
       	 ArenaConf.set("Name", arenaName);
       	 ArenaConf.set("MaxPlayers", maxPlayers);
       	 ArenaConf.set("MinPlayers", minPlayers);
       	 ArenaConf.set("location.warps.lobby", lobby);
       	 ArenaConf.set("location.warps.end", end);
       	 ArenaConf.set("rewards.money", money);
       	
       	for (Entry<String, Location> entry : spawnpoints.entrySet()) {
       		Location loc = entry.getValue();
       		
       		
       		int x = loc.getBlockX();
        	int y = loc.getBlockY();
        	int z = loc.getBlockZ();
        	float yaw = loc.getYaw();
        	float pitch = loc.getPitch();
        	String world = loc.getWorld().getName();
        	String spawn = world+","+x+","+y+","+z+","+pitch+","+yaw;
       		
       		ArenaConf.set("location.spawnpoints."+entry.getKey(), spawn);
       	}
       	 
       	 //Add a section for rewards
       	 
       	 
       	 try {
   			ArenaConf.save("plugins/SticksAndStones" + File.separator + "Arenas" + File.separator + arenaName + ".yml");
   		} catch (IOException e) {
   			e.printStackTrace();
   		}
       	@SuppressWarnings("unused")
		Arena arena = new Arena(arenaName, warp_lobby, spawnpoints, warp_end, maxPlayers, minPlayers, money);
       	arenaList.add(arenaName);
       }
        
        public void deleteArena(String arenaName) {
            
         File Arena;
       	 File ArenaFolder;
       	 ArenaFolder = new File(plugin.getDataFolder(), "Arenas");
       	 ArenaFolder.mkdir();
       	 Arena = new File(ArenaFolder, arenaName + ".yml");
       	 
       	 Arena.delete();
       	arenaList.remove(arenaName);
       	 
          
       }
        
        public void clearArmor(Player player){
        	player.getInventory().setHelmet(null);
        	player.getInventory().setChestplate(null);
        	player.getInventory().setLeggings(null);
        	player.getInventory().setBoots(null);
        	}
}
