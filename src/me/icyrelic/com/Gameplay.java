package me.icyrelic.com;


import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.UUID;

import me.icyrelic.com.Arenas.ArenaManager;
import me.icyrelic.com.Data.language;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class Gameplay extends BukkitRunnable{

	 
	private int secondsLeft;
	private ArrayList<String> players = new ArrayList<String>();
	private String arenaName;
	private ScoreboardManager manager = Bukkit.getScoreboardManager();
	private Scoreboard board = manager.getNewScoreboard();
	private Objective objective = board.registerNewObjective("sas", "sas_score");
	
	
	
	
	
	public Gameplay(ArrayList<String> arenaPlayers, int seconds, String arena){
		secondsLeft = seconds;
		players = arenaPlayers;
		arenaName = arena;
	}
	
	
	@SuppressWarnings("deprecation")
	public void run() {


		if(ArenaManager.getManager().getArena(arenaName).isInGame()){
			
			if(secondsLeft == 0){
				ArenaManager.getManager().endArena(arenaName);
				this.cancel();
			}else{
				updateScoreboard();
				for (String s: players) {
					Player p = Bukkit.getPlayer(s);
					
					p.setScoreboard(board);
					p.setLevel(secondsLeft);
				}
				
				
				
			}
			
			secondsLeft--;
		}else{
			this.cancel();
		}
		
		
		
		
	}
	
	public void updateScoreboard(){
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		objective.setDisplayName(language.format(language.arena_scoreboard_header, arenaName, "", "", 0, 0, 0));
		
		for (Entry<UUID, Integer> entry : ArenaManager.getManager().getArena(arenaName).getAllScores().entrySet()) {
			Player p = Bukkit.getPlayer(entry.getKey());
			Score score = objective.getScore(p.getName());
			score.setScore(ArenaManager.getManager().getArena(arenaName).getScore(p.getUniqueId())); 
   	 	}
		
		
		
		
		
	}
	    
	    
}
