package me.icyrelic.com;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
 
public class CleanItem {
 
    private Material material;
    private int amount = -1;
    private String name;
    private List<String> lores = new ArrayList<String>();
    private List<Enchant> enchs = new ArrayList<Enchant>();
    private short durability = -101;
    private MaterialData data;
 
    public CleanItem(Material par1) {
        material = par1;
    }
 
    public CleanItem(Material par1, int par2) {
        material = par1;
        amount = par2;
    }
 
    public CleanItem amount(int par1) {
        amount = par1;
        return this;
    }
 
    public CleanItem withName(String par1) {
        name = par1;
        return this;
    }
 
    public CleanItem withLores(String[] par1) {
        for (String s : par1) {
            lores.add(s);
        }
        return this;
    }
 
    public CleanItem withLore(String par1) {
        lores.add(par1);
        return this;
    }
 
    public CleanItem withEnchantment(Enchantment par1, int level, boolean glow) {
        enchs.add(new Enchant(par1, level, glow));
        return this;
    }
 
    public CleanItem withDurability(short par1) {
        durability = par1;
        return this;
    }
 
    public CleanItem withData(MaterialData par1) {
        data = par1;
        return this;
    }
 
    public ItemStack toItemStack() {
        ItemStack item = new ItemStack(material);
        if (amount != -1) {
            item.setAmount(amount);
        }
        ItemMeta meta = item.getItemMeta();
        if (name != null) {
            meta.setDisplayName(name);
        }
        if (!lores.isEmpty()) {
            meta.setLore(lores);
        }
        for (Enchant e : enchs) {
            meta.addEnchant(e.getEnchantment(), e.getLevel(), e.getGlow());
        }
        item.setItemMeta(meta);
        if (durability != -101) {
            item.setDurability(durability);
        }
        if (data != null) {
            item.setData(data);
        }
        return item;
    }
 
    public class Enchant {
 
        private Enchantment ench;
        private int level;
        private boolean glow;
 
        public Enchant(Enchantment par1, int par2, boolean par3) {
            ench = par1;
            level = par2;
            glow = par3;
        }
 
        public Enchantment getEnchantment() {
            return ench;
        }
 
        public int getLevel() {
            return level;
        }
 
        public boolean getGlow() {
            return glow;
        }
    }
 
}