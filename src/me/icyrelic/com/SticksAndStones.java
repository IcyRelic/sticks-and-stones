package me.icyrelic.com;

import java.io.File;

import me.icyrelic.com.Arenas.ArenaManager;
import me.icyrelic.com.Commands.command_sas;
import me.icyrelic.com.Data.language;
import me.icyrelic.com.Listeners.DamageEvents;
import me.icyrelic.com.Listeners.InventoryEvents;
import me.icyrelic.com.Listeners.QuitEvents;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class SticksAndStones extends JavaPlugin {
	
	public static Economy economy = null;
	public void onEnable(){
		
		loadConfiguration();
		langConfig();
		mkDirs();
		setupEconomy();
		getCommand("sticksandstones").setExecutor(new command_sas(this));
		ArenaManager.getManager().loadArenas();
		Bukkit.getServer().getPluginManager().registerEvents(new InventoryEvents(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new DamageEvents(this), this);
		Bukkit.getServer().getPluginManager().registerEvents(new QuitEvents(this), this);
		
		
	}
	
	
	private void mkDirs(){
		File arenas = new File("plugins/SticksAndStones/Arenas");
		arenas.mkdir();
	}
	
	private void loadConfiguration(){
	    getConfig().options().copyDefaults(true);
	    saveConfig();
	}
    
	private void langConfig(){
		
		if(!language.checkLangFile()){
			language.createLangFile();
			language.loadLangFile();
		}else{
			language.loadLangFile();
		}
		
	}
	private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }
	

}
