package me.icyrelic.com;


import java.util.ArrayList;

import me.icyrelic.com.Arenas.ArenaManager;
import me.icyrelic.com.Data.language;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class RunableCountdown extends BukkitRunnable{

	 
	private int secondsLeft;
	private ArrayList<String> players = new ArrayList<String>();
	private String arenaName;
	SticksAndStones plugin;
	public RunableCountdown(ArrayList<String> arenaPlayers, int seconds, String arena, SticksAndStones instance){
		secondsLeft = seconds;
		players = arenaPlayers;
		arenaName = arena;
		plugin = instance;
	}
	
	
	@SuppressWarnings("deprecation")
	public void run() {

		if(ArenaManager.getManager().getArena(arenaName).isCountdown() == false){
			ArenaManager.getManager().endArena(arenaName);
			this.cancel();
		}else{
			if(secondsLeft == 0){
				ArenaManager.getManager().startArena(arenaName, plugin);
				for (String s: players){
					Player p = Bukkit.getPlayer(s);
					p.playSound(p.getLocation(), Sound.NOTE_PLING, 10, 10);
				}
				this.cancel();
			}else{
				for (String s: players) {
					Player p = Bukkit.getPlayer(s);
					p.sendMessage(language.format(language.arena_countdown, arenaName, "", "", secondsLeft, 0, 0));
					p.playSound(p.getLocation(), Sound.NOTE_PLING, 10, 10);
				}
			}
			
			secondsLeft--;
		}
	}
	    
	    
}
