package me.icyrelic.com.Data;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class language {
	public static String plugin_information = "";
	public static String error_no_permission = "";
	public static String error_already_in_arena = "";
	public static String error_not_in_arena = "";
	public static String error_arena_already_exists = "";
	public static String error_arena_not_found = "";
	public static String error_not_editing = "";
	public static String error_must_be_int = "";
	public static String error_unknown_command = "";
	public static String error_not_player = "";
	public static String error_arena_ingame = "";
	public static String error_arena_full = "";
	public static String error_arena_not_enough_players = "";
	public static String error_use_tomahawk = "";
	public static String usage_sas_join = "";
	public static String usage_sas_arena = "";
	public static String usage_sas_arena_set = "";
	public static String usage_sas_arena_create = "";
	public static String usage_sas_arena_delete = "";
	public static String usage_sas_arena_edit = "";
	public static String usage_sas_arena_set_minplayers = "";
	public static String usage_sas_arena_set_maxplayers = "";
	public static String usage_sas_arena_set_reward_money = "";
	public static String usage_sas_force = "";
	public static String arena_created = "";
	public static String arena_fully_setup = "";
	public static String arena_deleted = "";
	public static String arena_editing = "";
	public static String arena_checkdata_message = "";
	public static String arena_checkdata_header = "";
	public static String arena_checkdata_settings = "";
	public static String arena_checkdata_warps = "";
	public static String arena_checkdata_spawnpoints = "";
	public static String arena_set_lobby = "";
	public static String arena_set_spawnpoint = "";
	public static String arena_set_end = "";
	public static String arena_set_minplayers = "";
	public static String arena_set_maxplayers = "";
	public static String arena_set_reward_money = "";
	public static String arena_list_no_arenas = "";
	public static String arena_list_header = "";
	public static String arena_list_list = "";
	public static String arena_players_no_players = "";
	public static String arena_players_header = "";
	public static String arena_players_list = "";
	public static String arena_force_start = "";
	public static String arena_force_end = "";
	public static String arena_player_joined = "";
	public static String arena_player_left = "";
	public static String arena_player_died = "";
	public static String arena_player_died_respawn_in = "";
	public static String arena_started = "";
	public static String arena_ended = "";
	public static String arena_winner = "";
	public static String arena_give_reward_money = "";
	public static String arena_scoreboard_header = "";
	public static String arena_countdown = "";
	
	
	
	public static boolean checkLangFile(){
		File Lang;
      	File sas;
        sas = new File("plugins/SticksAndStones");
      	sas.mkdir();
      	Lang = new File(sas, "lang.yml");
      	
      	if(Lang.exists()){
      		return true;
      	}else{
      		return false;
      	}
	}
	
	public static String Colorize(String s){
		ChatColor bold = ChatColor.BOLD;
	    ChatColor italic = ChatColor.ITALIC;
	    ChatColor underline = ChatColor.UNDERLINE;
	    ChatColor magic = ChatColor.MAGIC;
	    ChatColor strike = ChatColor.STRIKETHROUGH;
	    ChatColor reset = ChatColor.RESET;
	    return s.replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("&l", bold+"").replace("&r", reset+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+"");
	}
	
	public static String format(String s, String arenaName, String player, String list, int seconds, int money, int score){
		String varReplace = s.replace("%arenaName%", arenaName).replace("%winner%", player).replace("%player%", player).replace("%list%", list.replace("]", "").replace("[", "")).replace("%seconds%", seconds+"").replace("%money%", money+"").replace("%score%", score+"");
		ChatColor bold = ChatColor.BOLD;
	    ChatColor italic = ChatColor.ITALIC;
	    ChatColor underline = ChatColor.UNDERLINE;
	    ChatColor magic = ChatColor.MAGIC;
	    ChatColor strike = ChatColor.STRIKETHROUGH;
	    ChatColor reset = ChatColor.RESET;
		String Colorize = varReplace.replaceAll("(&([a-f0-9]))", "\u00A7$2").replace("&l", bold+"").replace("&r", reset+"").replace("&o", italic+"").replace("&n", underline+"").replace("&k", magic+"").replace("&m", strike+"");
		return Colorize;
	}
	
	
	public static void loadLangFile(){
		File Lang;
      	File sas;
        sas = new File("plugins/SticksAndStones");
      	sas.mkdir();
      	Lang = new File(sas, "lang.yml");
      	FileConfiguration LangConf = YamlConfiguration.loadConfiguration(Lang);
      	error_no_permission = LangConf.getString("error.no_permission");
      	error_already_in_arena = LangConf.getString("error.already_in_arena");
      	error_not_in_arena = LangConf.getString("error.not_in_arena");
      	error_arena_already_exists = LangConf.getString("error.arena_already_exists");
      	error_arena_not_found = LangConf.getString("error.arena_not_found");
      	error_not_editing = LangConf.getString("error.not_editing");
      	error_must_be_int = LangConf.getString("error.must_be_int");
      	error_unknown_command = LangConf.getString("error.unknown_command");
      	error_not_player = LangConf.getString("error.not_player");
      	error_arena_ingame = LangConf.getString("error.arena_ingame");
      	error_arena_full = LangConf.getString("error.arena_full");
      	error_arena_not_enough_players = LangConf.getString("error.arena_not_enough_players");
      	error_use_tomahawk = LangConf.getString("error.use_tomahawk");
      	
      	usage_sas_join = LangConf.getString("usage.sas_join");
      	usage_sas_arena = LangConf.getString("usage.sas_arena");
      	usage_sas_arena_set = LangConf.getString("usage.sas_arena_set");
      	usage_sas_arena_create = LangConf.getString("usage.sas_arena_create");
      	usage_sas_arena_delete = LangConf.getString("usage.sas_arena_delete");
      	usage_sas_arena_edit = LangConf.getString("usage.sas_arena_edit");
      	usage_sas_arena_set_minplayers = LangConf.getString("usage.sas_arena_set_minplayers");
      	usage_sas_arena_set_maxplayers = LangConf.getString("usage.sas_arena_set_maxplayers");
      	usage_sas_arena_set_reward_money = LangConf.getString("usage.sas_arena_set_reward_money");
      	usage_sas_force = LangConf.getString("usage.sas_force");
      	
      	arena_created = LangConf.getString("arena.created");
      	arena_fully_setup = LangConf.getString("arena.fully_setup");
      	arena_deleted = LangConf.getString("arena.deleted");
      	arena_editing = LangConf.getString("arena.editing");
      	arena_checkdata_message = LangConf.getString("arena.checkdata_message");
      	arena_checkdata_header = LangConf.getString("arena.checkdata_header");
      	arena_checkdata_settings = LangConf.getString("arena.checkdata_settings");
      	arena_checkdata_warps = LangConf.getString("arena.checkdata_warps");
      	arena_checkdata_spawnpoints = LangConf.getString("arena.checkdata_spawnpoints");
      	arena_set_lobby = LangConf.getString("arena.set_lobby");
      	arena_set_spawnpoint = LangConf.getString("arena.set_spawnpoint");
      	arena_set_end = LangConf.getString("arena.set_end");
      	arena_set_minplayers = LangConf.getString("arena.set_minplayers");
      	arena_set_maxplayers = LangConf.getString("arena.set_maxplayers");
      	arena_set_reward_money = LangConf.getString("arena.set_reward_money");
      	arena_list_no_arenas = LangConf.getString("arena.list_no_arenas");
      	arena_list_header = LangConf.getString("arena.list_header");
      	arena_list_list = LangConf.getString("arena.list_list");
      	arena_players_no_players = LangConf.getString("arena.players_no_players");
      	arena_players_header = LangConf.getString("arena.players_header");
      	arena_players_list = LangConf.getString("arena.players_list");
      	arena_force_start = LangConf.getString("arena.force_start");
      	arena_force_end = LangConf.getString("arena.force_end");
      	arena_player_joined = LangConf.getString("arena.player_joined");
      	arena_player_left = LangConf.getString("arena.player_left");
      	arena_player_died = LangConf.getString("arena.player_died");
      	arena_player_died_respawn_in = LangConf.getString("arena.player_died_respawn_in");
      	arena_started = LangConf.getString("arena.started");
      	arena_ended = LangConf.getString("arena.ended");
      	arena_winner = LangConf.getString("arena.winner");
      	arena_give_reward_money = LangConf.getString("arena.give_reward_money");
      	arena_scoreboard_header = LangConf.getString("arena.scoreboard_header");
      	arena_countdown = LangConf.getString("arena.countdown");
      	plugin_information = LangConf.getString("plugin.information");
	}
	
	public static void createLangFile(){
		File Lang;
      	File sas;
        sas = new File("plugins/SticksAndStones");
      	sas.mkdir();
      	Lang = new File(sas, "lang.yml");
      	
      	try {
      		Lang.createNewFile();
  		} catch (IOException e) {
  			e.printStackTrace();
  		}
      	FileConfiguration LangConf = YamlConfiguration.loadConfiguration(Lang);
      	LangConf.set("plugin.information", "&a%pluginName% v%version%");
      	LangConf.set("error.no_permission", "&cERROR: &4You do not have permission!");
      	LangConf.set("error.already_in_arena", "&cERROR: &4You must first leave your current arena");
      	LangConf.set("error.not_in_arena", "&cERROR: &4You are not in an arena");
      	LangConf.set("error.arena_already_exists", "&cERROR: &4An arena by that name already exists");
      	LangConf.set("error.arena_not_found", "&cERROR: &4Arena not found");
      	LangConf.set("error.not_editing", "&cERROR: &4You must be editing an arena");
      	LangConf.set("error.must_be_int", "&cERROR: &4Must be integer");
      	LangConf.set("error.unknown_command", "&cERROR: &4Unknown command use /sas help for a list of commands");
      	LangConf.set("error.not_player", "&cERROR: &4This command must be ran by a player");
      	LangConf.set("error.arena_ingame", "&cERROR: &4That arena is currently ingame!");
      	LangConf.set("error.arena_full", "&cERROR: &4That arena is currently full!");
      	LangConf.set("error.arena_not_enough_players", "&cERROR: &4There is not enough players in the arena");
      	LangConf.set("error.use_tomahawk", "&cERROR: &4You cannot use the tomahawk for another %seconds% seconds");
      	
      	LangConf.set("usage.sas_join", "&cUSAGE: &4/sas join [arena]");
      	LangConf.set("usage.sas_arena", "&cUSAGE: &4/sas arena <create | delete | edit | checkdata | set> [values]");
      	LangConf.set("usage.sas_arena_set", "&cUSAGE: &4/sas arena set <lobby | spawnpoint | end | maxplayers | minplayers | reward>");
      	LangConf.set("usage.sas_arena_create", "&cUSAGE: &4/sas arena create <name>");
      	LangConf.set("usage.sas_arena_delete", "&cUSAGE: &4/sas arena delete <name>");
      	LangConf.set("usage.sas_arena_edit", "&cUSAGE: &4/sas arena edit <name>");
      	LangConf.set("usage.sas_arena_set_minplayers", "&cUSAGE: &4/sas arena set minplayers <number>");
      	LangConf.set("usage.sas_arena_set_maxplayers", "&cUSAGE: &4/sas arena set maxplayers <number>");
      	LangConf.set("usage.sas_arena_set_reward_money", "&cUSAGE: &4/sas arena set reward money <number>");
      	LangConf.set("usage.sas_force", "&cUSAGE: &4/sas force <start | end> <arena>");
      	
      	LangConf.set("arena.created", "&aArena &6&l%arenaName% &ahas been created.");
      	LangConf.set("arena.fully_setup", "&aArena &6&l%arenaName% &ahas been been fully setup and is now ready to use.");
      	LangConf.set("arena.deleted", "&aArena &6&l%arenaName% &aDeleted.");
      	LangConf.set("arena.editing", "&aYou are now editing arena: &6&b%arenaName%");
      	LangConf.set("arena.checkdata_message", "&aType /sas arena checkdata for information");
      	LangConf.set("arena.checkdata_header", "&6Data needed for %arenaName%");
      	LangConf.set("arena.checkdata_settings", "&aSettings: &7%list%");
      	LangConf.set("arena.checkdata_warps", "&aWarps: &7%list%");
      	LangConf.set("arena.checkdata_spawnpoints", "&aSpawnpoints: &7%list%");
      	LangConf.set("arena.set_lobby", "&alobby warp set for arena &6%arenaName%");
      	LangConf.set("arena.set_spawnpoint", "&aspawnpoint added for arena &6%arenaName%");
      	LangConf.set("arena.set_end", "&aend warp set for arena &6%arenaName%");
      	LangConf.set("arena.set_minplayers", "&aminplayers set for arena &6%arenaName%");
      	LangConf.set("arena.set_maxplayers", "&amaxplayers set for arena &6%arenaName%");
      	LangConf.set("arena.set_reward_money", "&amoney set for arena &6%arenaName%");
      	LangConf.set("arena.list_no_arenas", "There are currently no arenas");
      	LangConf.set("arena.list_header", "&6Available Arenas:");
      	LangConf.set("arena.list_list", "&a%list%");
      	LangConf.set("arena.players_no_players", "There are currently no players in the arena");
      	LangConf.set("arena.players_header", "&6Players:");
      	LangConf.set("arena.players_list", "&a%list%");
      	LangConf.set("arena.force_start", "&aArena %arenaName% was forced to start");
      	LangConf.set("arena.force_end", "&aArena %arenaName% was forced to end");
      	LangConf.set("arena.player_joined", "&8%player% has joined he arena");
      	LangConf.set("arena.player_left", "&8%player% has left he arena");
      	LangConf.set("arena.player_died", "&9You died. You have become invisible.");
      	LangConf.set("arena.player_died_respawn_in", "&9You will respawn in %seconds% seconds...");
      	LangConf.set("arena.started", "&6Match Started");
      	LangConf.set("arena.ended", "&9The arena has ended");
      	LangConf.set("arena.winner", "&a%winner% has won! with a score of %score%");
      	LangConf.set("arena.give_reward_money", "&aYou have recieved $%money% for winning");
      	
      	LangConf.set("arena.scoreboard_header", "Arena: %arenaName%");
      	LangConf.set("arena.countdown", "&7Match starting in %seconds% seconds.");
      	
      	
      	try {
   			LangConf.save("plugins/SticksAndStones" + File.separator + "lang.yml");
   		} catch (IOException e) {
   			e.printStackTrace();
   		}
      	
	}
}
