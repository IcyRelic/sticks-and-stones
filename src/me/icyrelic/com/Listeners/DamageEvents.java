package me.icyrelic.com.Listeners;

import java.util.HashMap;

import me.icyrelic.com.SticksAndStones;
import me.icyrelic.com.Arenas.Arena;
import me.icyrelic.com.Arenas.ArenaManager;
import me.icyrelic.com.Data.language;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class DamageEvents implements Listener {
	
	private HashMap<String, Long> rTomahawked = new HashMap<String, Long>();
	SticksAndStones plugin;
	public DamageEvents(SticksAndStones instance) {

		plugin = instance;

	}
	@EventHandler
	public void onItemSpawn(ProjectileLaunchEvent e)
	{
		if(e.getEntity() instanceof Arrow){
			Arrow arrow = (Arrow) e.getEntity();
			if(arrow.getShooter() instanceof Player){
				Player p = (Player) arrow.getShooter();
				if(ArenaManager.getManager().getPlayersArena(p) != null){
					if(p.getItemInHand().getType() == Material.BOW){
						arrow.setMetadata("crossbow", new FixedMetadataValue(plugin, "crossbow"));
					}
					if(p.getItemInHand().getType() == Material.STONE_AXE){
						arrow.setMetadata("tomahawk", new FixedMetadataValue(plugin, "tomahawk"));
					}
				}
				
			}
		}
	}
	
    @EventHandler
    public void tomahawk(PlayerInteractEvent event){
        final Player player = event.getPlayer();
        if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
            if(player.getItemInHand().getType() == Material.STONE_AXE){
            	
            	if(ArenaManager.getManager().getPlayersArena(player) != null){
                	if(rTomahawked.containsKey(player.getName())) {
                		long secondsLeft = ((rTomahawked.get(player.getName())/1000)+10) - (System.currentTimeMillis()/1000);
                		if(secondsLeft>0) {
                			player.sendMessage(language.format(language.error_use_tomahawk, "", player.getName(), "", Integer.parseInt(Long.toString(secondsLeft)), 0, 0));
                		}else{
                			player.launchProjectile(Arrow.class);
                			rTomahawked.remove(player.getName());
                			rTomahawked.put(player.getName(), System.currentTimeMillis());
                		}
                   		
                	}else{
                		player.launchProjectile(Arrow.class);
            			rTomahawked.put(player.getName(), System.currentTimeMillis());
                	}

            	}
                		
            		
            }
        }
    }
    
    @EventHandler
    public void damage(EntityDamageEvent e){
    	if(e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if(ArenaManager.getManager().getPlayersArena(p) != null){
				e.setCancelled(true);
				
			}
    	}
    }
	
	
	@EventHandler
	public void playerDamage(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if(ArenaManager.getManager().getPlayersArena(p) != null){
				Arena arena = ArenaManager.getManager().getPlayersArena(p);
				if(e.getDamager().getType() == EntityType.ARROW){
					Arrow arrow = (Arrow) e.getDamager();
					if(arrow.getShooter() instanceof Player){
						Player shooter = (Player) arrow.getShooter();
						if(ArenaManager.getManager().getPlayersArena(shooter) != null){
							Arena shooterArena = ArenaManager.getManager().getPlayersArena(shooter);
							if(arena.getName().equals(shooterArena.getName())){
								System.out.print(arena.getDead().toString());
								if(!arena.getDead().contains(p.getName()) && !p.getName().equals(shooter.getName())){
									if(arrow.hasMetadata("tomahawk")){
										p.playSound(p.getLocation(), Sound.SPLASH, 10, 10);
										ArenaManager.getManager().processHumiliation(plugin,arena.getName(), shooter, p);
										e.setCancelled(true);

					            	}
					            	if(arrow.hasMetadata("crossbow")){
					            		shooter.playSound(p.getLocation(), Sound.LEVEL_UP, 10, 10);
					            		ArenaManager.getManager().processKill(plugin,arena.getName(), shooter, p);
					            		e.setCancelled(true);
					            	}
								}
							}
						}else{
							e.setCancelled(true);
						}
					}else{
						e.setCancelled(true);
					}
				}else{
					e.setCancelled(true);
				}
			}
			
		}
	}
	
	
	public void allowTomahawk(String name){
		rTomahawked.remove(name);
	}
	
	

}
