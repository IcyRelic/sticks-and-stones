package me.icyrelic.com.Listeners;

import me.icyrelic.com.SticksAndStones;
import me.icyrelic.com.Arenas.ArenaManager;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class InventoryEvents implements Listener {
	
	
	SticksAndStones plugin;
	public InventoryEvents(SticksAndStones instance) {

		plugin = instance;

	}
	
	@EventHandler
	public void pickup(PlayerPickupItemEvent e){
		Player p  = (Player) e.getPlayer();
		if(ArenaManager.getManager().getPlayersArena(p) != null){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void inventoryDrop(PlayerDropItemEvent e){
		Player p  = (Player) e.getPlayer();
		if(ArenaManager.getManager().getPlayersArena(p) != null){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void inventoryClick(InventoryClickEvent e){
		Player p  = (Player) e.getWhoClicked();
		if(ArenaManager.getManager().getPlayersArena(p) != null){
			e.setCancelled(true);
		}
	}

}
