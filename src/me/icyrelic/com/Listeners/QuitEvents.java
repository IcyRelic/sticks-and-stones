package me.icyrelic.com.Listeners;

import me.icyrelic.com.SticksAndStones;
import me.icyrelic.com.Arenas.Arena;
import me.icyrelic.com.Arenas.ArenaManager;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitEvents implements Listener{

	SticksAndStones plugin;
	public QuitEvents(SticksAndStones instance) {

		plugin = instance;

	}
	
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		if(ArenaManager.getManager().getPlayersArena(p) != null){
			Arena arena = ArenaManager.getManager().getPlayersArena(p);
			ArenaManager.getManager().removePlayer(p, arena.getName());
		}
		
	}
}
